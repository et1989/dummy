package main

import (
	"log"
	"net/http"
	"os"

	"mypkg"
)

func idxHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Dummy hello"))
	mypkg.Echo("Dummy hello")
}

func main() {
	http.HandleFunc("/", idxHandler)

	addr := os.Getenv("ADDR")

	if addr == "" {
		log.Fatalf("Addr not defined")
	}
        log.Println("Hello world")

	log.Fatal(http.ListenAndServe(addr, nil))
}
