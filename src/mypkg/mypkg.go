package mypkg

import (
	"fmt"
)


// Echo echo
func Echo(s string) {
	fmt.Println("Echo: ", s)
}
