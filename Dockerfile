FROM golang:1.10 as builder

COPY ./src /dummy/src
COPY ./Makefile /dummy/Makefile
WORKDIR /dummy

RUN make

FROM busybox:latest

COPY --from=builder /dummy/bin/dummy /usr/bin/dummy

CMD ["usr/bin/dummy"]
